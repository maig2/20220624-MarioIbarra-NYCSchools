import Foundation

enum NetworkError: LocalizedError, Error {
    case badURL
    case badDecode
    case badResponse
    case other
    
    var localizedDescription: String {
        switch self {
        case .badURL:
            return "There was an error creating the request to the server."
        case .badDecode:
            return "The data received from the server was invalid."
        case .badResponse:
            return "Invalid response from the server."
        default:
            return "Unable to complete your request. Please check your internet connection."
        }
        
        
    }
    
}

